FROM ubuntu:22.04

RUN  apt-get update \
&& DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install  --no-install-recommends -y ca-certificates git locales ansible  \
&& ansible-galaxy collection install community.general \
&& echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
&& locale-gen \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/* \
&& mkdir -p /root/.ssh \
&& chmod 700 /root/.ssh

ENV LC_ALL=en_US.UTF-8

COPY files/run.sh /run.sh

ENTRYPOINT ["/run.sh"]




