#!/bin/bash

# if a private key is supplied, start ssh agent and register key
if [[ -v SSH_PRIVATE_KEY ]]; then
  eval "$(ssh-agent -s)"
  echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
fi

# either provdide host keys...
if [[ -v SSH_SERVER_HOSTKEYS ]]; then
   echo "$SSH_SERVER_HOSTKEYS" > ~/.ssh/known_hosts
   chmod 644 ~/.ssh/known_hosts
fi

# ... or disable host key checking
if [[ -v DISABLE_HOST_KEY_CHECKING ]]; then
  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
fi

/usr/bin/ansible-playbook $*